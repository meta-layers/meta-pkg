# --> get ipk/gpg keys on the target

KEYSFORIPKSIGNINGPATH="/workdir/sources/keys-for-ipk-signing/backup"
FILESEXTRAPATHS_prepend := "${KEYSFORIPKSIGNINGPATH}:"

# Distro-specific keys can be added to this package in two ways:
#
#   1) In a .bbappend, add .gpg and/or .asc files to SRC_URI and install them to
#      ${D}${datadir}/opkg/keyrings/ in a do_install_append function. These
#      files should not be named 'key-$name.gpg' to ensure they don't conflict
#      with keys exported as per (2).
#
#   2) In a .bbappend, distro config or local.conf, override the variable
#      OPKG_KEYRING_KEYS to contain a space-separated list of key names. For
#      each name, 'gpg --export $name' will be ran to export the public key to a
#      file named 'key-$name.gpg'. The public key must therefore be in the gpg
#      keyrings on the build machine.

# Note: for opkg and gpg signed packages it looks like it's not
#       1) or 2) but 1) and 2)
#       both methods seem to be needed to get a public key on the target

SRC_URI += "\
            file://ipkkeypub.gpg \
            "
do_install_append () {
	install -m 0644 ${WORKDIR}/ipkkeypub.gpg ${D}${datadir}/opkg/keyrings/
}

# I introduced this variable to pass it to the postinstall script
IPKKEY="ipkkey"

OPKG_KEYRING_KEYS = "${IPKKEY}"

# <-- get ipk/gpg keys on the target

# --> postinstall script to call opkg-key trust

# append to the postinstall script our new trust command
# please note that the new command to opkg-key was added in opkg_%.bbappend

pkg_postinst_ontarget_${PN}_append () {
    if test -x ${bindir}/opkg-key
    then
        ${bindir}/opkg-key trust ${IPKKEY}
    fi
}

# <-- postinstall script to call opkg-key trust
