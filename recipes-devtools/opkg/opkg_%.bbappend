# we want gpg support:

#PACKAGECONFIG ??= "libsolv"
#
#PACKAGECONFIG[gpg] = "--enable-gpg,--disable-gpg,\
#    gnupg gpgme libgpg-error,\
#    ${@ "gnupg" if ("native" in d.getVar("PN")) else "gnupg-gpg"}\
#    "
#PACKAGECONFIG[curl] = "--enable-curl,--disable-curl,curl"
#PACKAGECONFIG[ssl-curl] = "--enable-ssl-curl,--disable-ssl-curl,curl openssl"
#PACKAGECONFIG[openssl] = "--enable-openssl,--disable-openssl,openssl"
#PACKAGECONFIG[sha256] = "--enable-sha256,--disable-sha256"
#PACKAGECONFIG[libsolv] = "--with-libsolv,--without-libsolv,libsolv"

# bitbake opkg -e | grep ^PACKAGECONFIG=
# PACKAGECONFIG="libsolv"

# let's add all the options:
PACKAGECONFIG_append += " gpg curl ssl-curl openssl sha256"
#PACKAGECONFIG_append += " gpg"

# we want to modify opkg.conf

# -->
# Enable per package signatures
# option check_pkg_signature 1
#
# Enable ascii armoured signatures .asc
# option signature_type gpg-asc
# <--

do_install_append() {
        # add some options after this line in opkg.conf
        #     #option proxy_password <password>
        # those options enable per package (gpg) signatures and disable ASCII armored signatures, which means binary signatures .sig per package

        sed -i '/^#option proxy_password <password>/a \\n# --> pick one\n# Note: needs to match with IPK_GPG_SIGNATURE_TYPE, PACKAGE_FEED_GPG_SIGNATURE_TYPE\n\n# Enable Binary signatures .sig\noption signature_type gpg\n\n# Enable ASCII armored signatures .asc\n#option signature_type gpg-asc\n# <-- pick one\n\n# Enable per package signatures\noption check_pkg_signature 1' ${D}${sysconfdir}/opkg/opkg.conf

        sed -i 's/# Enable GPGME signature/# Enable package feeds with (gpg) signatures/' ${D}${sysconfdir}/opkg/opkg.conf

        # this option enables a signed package feed
        # with the signature_type gpg or default above .sig and not .asc
        sed -i 's/# option check_signature 1/option check_signature 1/' ${D}${sysconfdir}/opkg/opkg.conf

        # we add to /usr/bin/opkg-key a trust command
        # after this line:
        #       echo "  populate            - Import keys from /usr/share/opkg/keyrings"
        # this line:
        #       echo "  trust <keyid>       - Trust the key <keyid>"
        sed -i '/Import keys from \/usr\/share\/opkg\/keyrings/a\    \echo "  trust <keyid>       - Trust the key <keyid>"' ${D}${bindir}/opkg-key
        # insert before populate) --> trust)
        sed -i '/populate)/i\   \ trust)' ${D}${bindir}/opkg-key
        # insert after trust)
        #       (echo trust &echo 5 &echo y &echo quit) | $GPG --command-fd 0 --edit-key "$1" 
        #       ;;
        # note that in my case the key is ipkkey
        sed -i '/trust)/a\       \ (echo trust &echo 5 &echo y &echo quit) | $GPG --command-fd 0 --edit-key "$1"\n\       \ ;;' ${D}${bindir}/opkg-key
}

